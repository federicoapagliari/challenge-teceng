const { Router } = require('express');
const router = Router();

const roleController = require('../controllers/role');

// Ruta para obtener todos los roles del sistema
router.get('/', roleController.getRoles);

// Ruta para crear un rol en el sistema
router.post('/', roleController.createRole);



module.exports = router;