const { Router } = require('express');
const router = Router();

const playerActionRouter = require('./playerActions');

const { authMiddleware } = require('../middlewares/isAuthenticated');
const matchController = require('../controllers/match');

// Ruta para crear un nuevo partido
router.post('/', authMiddleware, matchController.createMatch);

// Ruta para obtener oponentes
router.get('/opponentsAlreadyPlayed/:playerId', matchController.getPlayedOpponents);

router.get('/opponentsToPlay/:playerId', matchController.getOpponentsToFace);

// Ruta para obtener una lista de partidos
router.get('/', authMiddleware, matchController.getMatches);

// Ruta para obtener una lista de partidos ganados
router.get('/won/:playerId', authMiddleware, matchController.getWinnerMatchs);

// Ruta para obtener una lista de partidos perdidos
router.get('/lost/:playerId', authMiddleware, matchController.getLostMatchs);


// Ruta para obtener detalles de un partido específico
router.get('/:matchId', authMiddleware, matchController.getMatchDetails);

// TODO
// Ruta para acciones de un jugador dentro de un partido específico
router.use('/:matchId/player-actions', playerActionRouter);



module.exports = router;