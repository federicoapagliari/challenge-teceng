const { Router } = require('express');
const router = Router();

const { authMiddleware } = require('../middlewares/isAuthenticated');
const playerActionController = require('../controllers/playerActions');

router.post('/', authMiddleware, playerActionController.createPlayerAction);
router.get('/', authMiddleware, playerActionController.getAllActions);
router.get('/details/:matchId/:userId', authMiddleware, playerActionController.getPlayerActionsInMatch);




module.exports = router;