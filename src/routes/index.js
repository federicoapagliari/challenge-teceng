const { Router } = require('express');
const router = Router();

// Import routers
const userRouter = require('./user');
const roleRouter = require('./role');
const matchRouter = require('./match');
const playerActionsRouter = require('./playerActions');


router.use('/user/', userRouter);
router.use('/role/', roleRouter);
router.use('/match/', matchRouter);
router.use('/actions/', playerActionsRouter);

module.exports = router 