const { Router } = require('express');
const router = Router();

const { isAdmin } = require('../middlewares/isAdmin');
const { authMiddleware } = require('../middlewares/isAuthenticated');

const userController = require('../controllers/user');

// Ruta para registrar usuarios
router.post('/signUp', authMiddleware, isAdmin, userController.registerUser);

// Ruta para iniciar sesión
router.post('/signIn', userController.loginUser);

// Ruta para refrescar accesToken
router.post('/refresh', authMiddleware, userController.refreshToken);


module.exports = router;