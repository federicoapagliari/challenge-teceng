const { DataTypes } = require('sequelize');
const sequelize = require('../config/sequelize');

const Match = sequelize.define('Match', {
    player1Id: {
        type: DataTypes.INTEGER,
        allowNull: false,
        references: {
            model: 'User',
            key: 'id'
        }
    },
    player2Id: {
        type: DataTypes.INTEGER,
        allowNull: false,
        references: {
            model: 'User',
            key: 'id'
        }
    },
    startTime: {
        type: DataTypes.DATE,
        allowNull: false
    },
    endTime: {
        type: DataTypes.DATE,
        allowNull: true
    },
    winnerId: {
        type: DataTypes.INTEGER,
        allowNull: true,
        references: {
            model: 'User',
            key: 'id'
        }
    },
});

// Método para asociar los modelos
Match.associate = (models) => {
    Match.hasMany(models.PlayerAction, { foreignKey: 'matchId', sourceKey: 'id' });
};



module.exports = Match;