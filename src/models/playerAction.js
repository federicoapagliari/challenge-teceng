const { DataTypes } = require('sequelize');
const sequelize = require('../config/sequelize');

const PlayerAction = sequelize.define('PlayerAction', {
    actionType: {
        type: DataTypes.ENUM('ENVIDO', 'FLOR', 'FALTA_ENVIDO', 'REAL_ENVIDO'),
        allowNull: false
    },
    userId: {
        type: DataTypes.INTEGER,
        allowNull: false,
        references: {
            model: 'User',
            key: 'id'
        }
    },
    matchId: {
        type: DataTypes.INTEGER,
        allowNull: false,
        references: {
            model: 'Match',
            key: 'id'
        }
    },
    point: {
        type: DataTypes.INTEGER,
        allowNull: false
    },
    isWinner: {
        type: DataTypes.BOOLEAN,
        allowNull: false
    }
});

// Método para asociar los modelos
PlayerAction.associate = (models) => {
    PlayerAction.belongsTo(models.Match, { foreignKey: 'matchId', targetKey: 'id' });
};



module.exports = PlayerAction;