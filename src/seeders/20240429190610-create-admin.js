'use strict';
const bcrypt = require('bcryptjs');

module.exports = {

  async up(queryInterface, Sequelize) {
    const adminRole = await queryInterface.rawSelect('Role', {
      where: { name: 'administrador' }
    }, ['id']);

    if (!adminRole) {
      throw new Error('Rol de administrador no encontrado');
    }

    const hashedPassword = await bcrypt.hash("password", 10);

    await queryInterface.bulkInsert('User', [{
      email: "admin@gmail.com",
      name: "admin",
      password: hashedPassword,
      dateOfBirth: new Date(),
      roleId: adminRole,
      createdAt: new Date(),
      updatedAt: new Date(),
    }])

  },

  async down(queryInterface, Sequelize) {

    await queryInterface.bulkDelete('User', null, {})
  }
};
