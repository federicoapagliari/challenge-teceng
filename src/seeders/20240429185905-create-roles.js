'use strict';

module.exports = {
  async up(queryInterface, Sequelize) {
    return queryInterface.bulkInsert('Role', [
      {
        name: 'administrador',
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        name: 'consultor',
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        name: 'jugador',
        createdAt: new Date(),
        updatedAt: new Date(),
      },
    ]);
  },

  async down(queryInterface, Sequelize) {
    await queryInterface.bulkDelete('Role', null, {})

  }
};
