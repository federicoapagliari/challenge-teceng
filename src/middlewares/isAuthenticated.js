const jwt = require('jsonwebtoken');
const User = require('../models/user');

const authMiddleware = async (req, res, next) => {
    // Obtener el token del encabezado de autorización
    const token = req.headers.authorization;

    // Verificar si hay un token
    if (!token) {
        return res.status(401).json({ message: 'Token no proporcionado. Debes iniciar sesión.' });
    }

    try {
        // Verificar y decodificar el token
        const decoded = jwt.verify(token, process.env.JWT_SECRET);

        // Agregar la información del usuario al objeto req
        const user = await User.findOne({ id: decoded.userId })
        req.user = user.dataValues;

        next();
    } catch (error) {
        console.error('Error al verificar el token:', error);
        return res.status(401).json({ message: 'Token inválido o expirado. Debes iniciar sesión nuevamente.' });
    }
};

module.exports = { authMiddleware };