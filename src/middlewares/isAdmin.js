const Role = require('../models/role');

// Middleware de verificación de rol de administrador
const isAdmin = async (req, res, next) => {
    const adminRole = await Role.findOne({
        where: { name: 'administrador' }
    }, ['id']);
    console.log(adminRole.id)
    if (req.user.roleId !== adminRole.id) {
        return res.status(403).json({ message: 'Unauthorized access. Only administrators can perform this action.' });
    }
    next();
};

module.exports = { isAdmin }