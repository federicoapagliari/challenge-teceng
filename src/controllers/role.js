const Role = require('../models/role');

const getRoles = async (req, res) => {
    try {
        const roles = await Role.findAll();

        res.status(200).json({ roles });

    } catch (error) {
        console.error('Error getRoles: ', error);
        res.status(500).json({ message: 'Error getting roles' });
    }
}


const createRole = async (req, res) => {
    try {
        const { name } = req.body;
        // TODO: AGREGAR VALIDAR PARA NO REPETIR ROL
        const newRole = await Role.create({ name });

        res.status(201).json({ newRole });
    } catch (error) {
        console.error('Error createRole: ', error);
        res.status(500).json({ message: 'Error creating role' });
    }
}


module.exports = {
    getRoles,
    createRole
}