const Match = require('../models/match');
const User = require('../models/user');
const Role = require('../models/role');
const { Op } = require('sequelize');

const isPlayerInMatch = async (playerId) => {
    try {
        const activeMatch = await Match.findOne({
            where: {
                [Op.and]: [
                    { [Op.or]: [{ player1Id: playerId }, { player2Id: playerId }] },
                    { endTime: null } // El partido aún no ha terminado
                ]
            }
        });

        return !!activeMatch; 
    } catch (error) {
        console.error('Error isPlayerInMatch: ', error);
        return false;
    }
};


const createMatch = async (req, res) => {
    try {

        const { player1Id, player2Id, startTime, endTime, winnerId } = req.body;

        // Verificar si los jugadores ya están en un partido
        const player1InMatch = await isPlayerInMatch(player1Id);
        const player2InMatch = await isPlayerInMatch(player2Id);

        // TODO: AGREGAR VALIDACION PARA NO REPETIR PARTIDOS

        const match = await Match.create({
            player1Id,
            player2Id,
            startTime,
            endTime,
            winnerId,
        });
        res.status(201).json({ match });


    } catch (error) {
        console.error('Error createMatch: ', error);
        res.status(500).json({ message: 'Error creating match' });
    }
}

const getMatchDetails = async (req, res) => {
    try {
        const { matchId } = req.params;

        const match = await Match.findByPk(matchId);

        if (!match) {
            return res.status(404).json({ error: 'Match not found' });
        }

        res.status(200).json({ match });
    } catch (error) {
        console.error('Error getMatchDetails: ', error);
        res.status(500).json({ message: 'Error getting match details' });
    }
}

const getMatches = async (req, res) => {
    try {
        const matches = await Match.findAll();

        res.status(200).json({ matches });
    } catch (error) {
        console.error('Error getMatches:', error);
        res.status(500).json({ message: 'Error getting matches' });
    }
}

const getPlayedOpponents = async (req, res) => {
    try {
        const { playerId } = req.params;
        const parsedId = parseInt(playerId)


        const matches = await Match.findAll({
            where: {
                [Op.or]: [
                    { player1Id: parsedId },
                    { player2Id: parsedId }
                ]
            },
            attributes: ['player1Id', 'player2Id']
        });

        const playedOpponents = matches.flatMap(match => [match.player1Id, match.player2Id]);
        const alreadyPlayed = playedOpponents.filter(players => players !== parsedId);

        res.status(200).json({
            alreadyPlayed
        });

    } catch (error) {
        console.error("Error getPlayedAndRemainingOpponents: ", error);
        res.status(500).json({
            message: 'Error retrieving player matches'
        });
    }

};

const getOpponentsToFace = async (req, res) => {
    try {
        const { playerId } = req.params;
        const parsedId = parseInt(playerId)


        const matches = await Match.findAll({
            where: {
                [Op.or]: [
                    { player1Id: parsedId },
                    { player2Id: parsedId }
                ]
            },
            attributes: ['player1Id', 'player2Id']
        });

        const playedOpponents = matches.flatMap(match => [match.player1Id, match.player2Id]);
        const alreadyPlayed = playedOpponents.filter(players => players !== parsedId);

        const rolePlayerId = await Role.findOne({
            where: { name: 'jugador' },
            attributes: ['id']
        });

        const unmatchedOpponents = await User.findAll({
            where: {
                id: { [Op.notIn]: alreadyPlayed, [Op.ne]: parsedId },
                roleId: rolePlayerId.id
            }
        });

        res.status(200).json({
            unmatchedOpponents
        });
    }

    catch (error) {
        console.error("Error getOpponentsToFace: ", error);
        res.status(500).json({
            message: 'Error retrieving player future oponents'
        });
    }
}

const getWinnerMatchs = async (req, res) => {
    try {
        const { playerId } = req.params;

        const winnerMatchs = await Match.findAll({
            where: {
                [Op.or]: [
                    { winnerId: playerId },
                ]
            },
        });

        res.status(200).json({ winnerMatchs })
    } catch (error) {
        console.error("Error getWinnerMatchs: ", error);
        res.status(500).json({
            message: 'Error retrieving player victories'
        });
    }

}

const getLostMatchs = async (req, res) => {
    try {
        const { playerId } = req.params;
        const parsedId = parseInt(playerId)


        const allMatchesWherePlayerId = await Match.findAll({
            where: {
                [Op.or]: [
                    { player1Id: playerId },
                    { player2Id: playerId }
                ]
            },
        });

        const lostMatches = allMatchesWherePlayerId.filter(match => match.winnerId !== parsedId);

        res.status(200).json({ lostMatches })
    } catch (error) {
        console.error("Error getLostMatchs: ", error);
        res.status(500).json({
            message: 'Error retrieving player defeats'
        });
    }

}


module.exports = {
    createMatch,
    getMatchDetails,
    getMatches,
    getPlayedOpponents,
    getOpponentsToFace,
    getWinnerMatchs,
    getLostMatchs
};
