const PlayerAction = require('../models/playerAction');
const Match = require('../models/match');
const User = require('../models/user');


const createPlayerAction = async (req, res) => {
    try {
        const { actionType, matchId, userId, point, isWinner } = req.body;


        const playerAction = await PlayerAction.create({ actionType, matchId, userId, point, isWinner });

        res.status(201).json({ playerAction });
    } catch (error) {
        console.error('Error createPlayerAction: ', error);
        res.status(500).json({ message: 'Could not create player action' });
    }
};

const getAllActions = async (req, res) => {
    try {
        const actions = await PlayerAction.findAll();

        res.status(200).json({ actions });
    } catch (error) {
        console.error('Error getAllActions: ', error);
        res.status(500).json({ message: 'Could not get player actions' });
    }
}


const getPlayerActionsInMatch = async (req, res) => {
    try {
        const { matchId, userId } = req.params;

        const playerActions = await PlayerAction.findAll({
            where: { matchId, userId },
            include: [Match]
        });

        res.status(200).json({ playerActions });
    } catch (error) {
        console.error('Error getPlayerActionsInMatch: ', error);
        res.status(500).json({ message: 'Could not fetch player actions' });
    }
};




module.exports = {
    createPlayerAction,
    getAllActions,
    getPlayerActionsInMatch
};