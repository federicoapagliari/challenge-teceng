const User = require('../models/user');
const Role = require('../models/role');

const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');

const registerUser = async (req, res) => {
    try {
        const { email, name, password, dateOfBirth, roleId } = req.body;

        const existingUser = await User.findOne({ where: { email } });
        if (existingUser) {
            return res.status(400).json({ message: 'User Already Exist' });
        }

        const newUser = await User.create({ email, name, password, dateOfBirth, roleId });

        res.status(201).json({ newUser });
    } catch (error) {
        console.error('Error registerUser: ', error);
        res.status(500).json({ message: 'Error attempting to register user' });
    }
};

const loginUser = async (req, res) => {
    try {
        const { email, password } = req.body;

        const user = await User.findOne({ where: { email } });

        if (!user) {
            return res.status(404).json({ message: "User not found" });
        }

        const isMatch = await bcrypt.compare(password, user.password);
        if (!isMatch) {
            return res.status(401).json({ message: "Invalid credentials" });
        }

        const adminRole = await Role.findOne({
            id: user.roleId,
            attributes: ['name']
        }
        );

        const token = jwt.sign({ userId: user.id, userName: user.name, userRole: adminRole },
            process.env.JWT_SECRET, { expiresIn: '1h' });

        res.status(200).json({ token });
    } catch (error) {
        console.error('Error loginUser: ', error);
        res.status(500).json({ message: 'Error signing in' });

    }
};

const refreshToken = async (req, res) => {
    const token = req.headers.authorization;
    if (!token) {
        return res.status(401).json({ message: 'Autenthication is requerided' });
    }
    try {
        const payload = jwt.verify(token, process.env.JWT_SECRET);

        const nuevoToken = jwt.sign({ userId: payload.usedId }, process.env.JWT_SECRET, { expiresIn: '1h' });

        res.json({ token: nuevoToken });
    } catch (error) {
        if (error instanceof jwt.TokenExpiredError) {
            return res.status(401).json({ message: 'Expired Token' });
        }
        console.error('Error refreshToken: ', error);
        return res.status(401).json({ message: 'Invalid Token' });
    }
}


module.exports = {
    registerUser,
    loginUser,
    refreshToken
};