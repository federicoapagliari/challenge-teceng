const express = require('express');
const cors = require('cors');

const router = require('./routes/index');

const app = express();

app.use(express.json());
app.use(cors());

app.use("/app", router);

app.listen(process.env.SERVER_PORT, () => {
    console.log(`Server is running on port ${process.env.SERVER_PORT}`)
})